# Copyright (C) 2018 Alex Smith
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "html"

require "twitter-crystal"
require "cryss"

require "./helper"
require "./twitter"

module Aviary
  module Feed
    include Helper

    URL_CAP = /(https?:\/\/(www\.)?[-a-zA-Z0-9]+\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*))/

    def generate_feed(client : Client, tweets : Array(Twitter::Tweet | ExtendedTweet), user : String? = nil) : RSS::Channel
      url = URI.parse fetch_env "BASE_URL"

      desc = "Twitter feed"
      if u = user
        puts "rss: building single feed"
        desc += " for #{u}"
      else
        desc = "Aggregated " + desc
      end

      channel = RSS::Channel.new(
        link: url,
        title: "aviary",
        description: desc
      )
      channel.language = "en-au"
      channel.last_build_date = Time.now
      channel.category = [RSS::Category.new "Twitter"]
      channel.ttl = 15 * 60

      # chronological order
      tweets.sort! { |a, b| b.created_at <=> a.created_at }

      tweets.each do |tweet|
        user = tweet.user.as Twitter::User
        url = URI.parse "https://twitter.com/#{user.screen_name}/status/#{tweet.id}"
        text =
          case tweet
          when Twitter::Tweet
            tweet.text
          when ExtendedTweet
            tweet.full_text
          else
            ""
          end

        item = RSS::Item.new(title: "Status from #{user.name}")
        item.description = "<p>" + escape_html(text) + "</p>"
        item.author = "@" + user.screen_name
        item.link = url
        item.guid = url
        item.pub_date = tweet.created_at
        item.source = "Twitter"
        item.source_url = URI.parse "https://twitter.com/"

        channel << item
      end

      channel
    end

    def escape_html(str : String) String
      # str = str.gsub(URL_CAP) do |url|
      #   href = url.starts_with?("https://t.co/") ? "continued" : url
      #   "<a href=\"#{url}\">#{href}</a>"
      # end
      # HTML.escape str
      # str = str.gsub({ '<' => "&lt;", '>' => "&gt;" })
      # str = str.gsub("&quot;") { "\"" }
      str
    end
  end
end
