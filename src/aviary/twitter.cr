# Copyright (C) 2018 Alex Smith
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "twitter-crystal"
require "json"

require "./helper"
require "./db"

module Aviary
  class ExtendedTweet
    JSON.mapping({
      created_at:              {type: Time, converter: Time::Format.new("%a %b %d %T +0000 %Y")},
      favorite_count:          Int32?,
      favorited:               Bool,
      id:                      Int64,
      in_reply_to_screen_name: String?,
      in_reply_to_status_id:   Int64?,
      in_reply_to_user_id:     Int64?,
      lang:                    String?,
      place:                   Twitter::Place?,
      retweet_count:           Int32,
      retweeted:               Bool,
      retweeted_status:        Twitter::Tweet?,
      source:                  String,
      full_text:               String,
      truncated:               Bool,
      user:                    Twitter::User?,
    })
    def_equals id
  end

  class Client
    include Helper

    getter handle : Twitter::REST::Client
    getter users  : Set(String)

    def initialize
      consumer_key    = fetch_env "CONSUMER_KEY"
      consumer_secret = fetch_env "CONSUMER_SECRET"
      access_token    = fetch_env "ACCESS_TOKEN"
      access_secret   = fetch_env "ACCESS_SECRET"

      @users = Set.new [] of String
      @handle = Twitter::REST::Client.new(
        consumer_key,
        consumer_secret,
        access_token,
        access_secret
      )
    end

    def lookup(ids : Array(Int32 | Int64), options = {} of String => String) : Array(Twitter::Tweet)
      response = @handle.get("/1.1/statuses/lookup.json",
                             options.merge({"id" => ids.map(&.to_s).join(",")}))
      Array(Twitter::Tweet).from_json(response)
    end

    def lookup_ext(ids : Array(Int32 | Int64), options = {} of String => String) : Array(ExtendedTweet)
      response = @handle.get("/1.1/statuses/lookup.json",
                             options.merge({"id" => ids.map(&.to_s).join(","),
                                            "tweet_mode" => "extended"}))
      Array(ExtendedTweet).from_json(response)
    end

    def user_timeline_ext(screen_name : String, options = {} of String => String) : Array(ExtendedTweet)
      response = @handle.get("/1.1/statuses/user_timeline.json",
                             options.merge({"screen_name" => screen_name,
                                            "tweet_mode" => "extended"}))
      Array(ExtendedTweet).from_json(response)
    end

    def fetch(retweets : Bool = false) : Array(Twitter::Tweet)
      tweets = [] of Twitter::Tweet
      puts "twitter: updating [#{users.join ", "}]"
      @users.each do |user|
        puts "twitter: fetching @#{user}"
        tweets += fetch user, retweets
      end
      tweets
    end

    def fetch_ext(retweets : Bool = false) : Array(ExtendedTweet)
      tweets = [] of ExtendedTweet
      puts "twitter: updating [#{users.join ", "}]"
      @users.each do |user|
        puts "twitter: fetching @#{user}"
        tweets += fetch_ext user, retweets
      end
      tweets
    end

    def fetch(user : String, retweets : Bool = false) : Array(Twitter::Tweet)
      options = {"include_rts" => retweets.to_s}
      @handle.user_timeline user, options
    end

    def fetch_ext(user : String, retweets : Bool = false) : Array(ExtendedTweet)
      options = {"include_rts" => retweets.to_s}
      user_timeline_ext user, options
    end
  end
end
