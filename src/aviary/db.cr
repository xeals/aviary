# Copyright (C) 2018 Alex Smith
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "db"
require "sqlite3"

require "./helper"

module Aviary
  class Store
    include Helper

    property handle : DB::Database

    def initialize(path : String? = nil)
      path ||= optional "DB"
      path ||= "./aviary.db"

      @handle = DB.open "sqlite3://#{path}"

      begin
        @handle.query "SELECT name FROM users LIMIT 1"
      rescue
        create_tables
      end
    end

    def create_tables
      @handle.transaction do |tx|
        tx.connection.exec "CREATE TABLE users (name TEXT UNIQUE)"
        tx.connection.exec "CREATE TABLE tweets (id INTEGER UNIQUE)"
      end
    end

    def last_tweet : Int64
      begin
        @handle.query_one "SELECT MAX(id) FROM tweets", as: Int64
      rescue
        return 1_i64
      end
    end

    def load_users : Array(String)
      rows = [] of String
      @handle.query "SELECT name FROM users" do |rs|
        rs.each do
          rows << rs.read(String)
        end
      end
      rows
    end

    def save_users(users : Array(String))
      @handle.transaction do |tx|
        tx.connection.exec "DELETE FROM users"
        users.each do |user|
          tx.connection.exec "INSERT OR IGNORE INTO users VALUES (?)", user
        end
      end
    end

    def load_tweets : Array(Int64)
      rows = [] of Int64
      @handle.query "SELECT id FROM tweets" do |rs|
        rs.each do
          rows << rs.read(Int64)
        end
      end
      rows
    end

    def save_tweets(tweets : Array(Int64))
      @handle.transaction do |tx|
        tweets.each do |tweet|
          tx.connection.exec "INSERT OR IGNORE INTO tweets VALUES (?)", tweet
        end
      end
    end
  end
end
