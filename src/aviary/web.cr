# Copyright (C) 2018 Alex Smith
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require "kemal"
require "kilt/slang"

require "./db"
require "./helper"
require "./rss"
require "./twitter"

module Aviary
  class WebServer
    include Helper
    include Feed

    def initialize
      if port = optional "PORT"
        Kemal.config.port = port.to_i
      end
      if host = optional "HOST"
        Kemal.config.host_binding = host
      end
      @db = Store.new
      @client = Client.new

      @db.load_users.each do |user|
        @client.users << user
      end

      # p @client.lookup([1049979227859107840])

      # GET / => settings
      get "/" do |env|
        prefill = @db.load_users.join("\n")
        render "templates/settings.slang"
      end

      # POST / => update settings
      post "/" do |env|
        users = env.params.body["users"].as(String).split
        puts "web: saving users [#{users.join ", "}]"
        @db.save_users users
        users.each do |user|
          @client.users << user
        end
        env.redirect "/"
      end

      # GET /:user => RSS for single user
      get "/:user" do |env|
        user = env.params.url["user"]
        puts "web: getting single user @#{user}"
        tweets = @client.fetch_ext user
        feed = generate_feed(@client, tweets, user)
        feed.to_s env.response.output
      end

      # GET /feed.xml => RSS
      get "/feed.xml" do |env|
        puts "web: getting all configured users"
        tweets = @client.fetch_ext
        feed = generate_feed(@client, tweets)
        feed.to_s env.response.output
      end
    end

    def run
      Kemal.run
    end
  end
end
