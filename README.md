# aviary

`aviary` is a light Twitter-to-RSS relay intended to make it easy for people to read Twitter feeds from their RSS client.

## Installation

Included is a Dockerfile for easy deployment, or you can build it yourself.

### From source

You need the Crystal compiler, which may be provided by your distribution.

```sh
$ git clone https://gitlab.com/xeals/aviary
$ cd aviary
$ shards install
$ shards build
$ ./bin/aviary
```

### From Docker

```sh
$ git clone https://gitlab.com/xeals/aviary
$ cd aviary
$ docker build -t aviary:latest .
$ docker run aviary:latest
```

### Configuring

`aviary` is configured through environment variables.

### Required

- `AVIARY_BASE_URL` : URL of the site you'll be running aviary from. Not super important as to what it is, but still required.

#### Twitter API

As `aviary` fetches information using the Twitter API, you are required to be able to access the API. Follow the instructions on the [Twitter API guide](https://developer.twitter.com/en.html) and create a new app.

- `AVIARY_CONSUMER_KEY` : Consumer API key
- `AVIARY_CONSUMER_SECRET` : Consumer API secret
- `AVIARY_ACCESS_TOKEN` : Access token
- `AVIARY_ACCESS_SECRET` : Access token secret

### Optional

- `AVIARY_DB` : SQLite database file. Defaults to `./aviary.db`
- `AVIARY_PORT` : Port to run on. Defaults to `3000`.
- `AVIARY_HOST` : Host to listen on. Defaults to `0.0.0.0`.

### Usage

`aviary` runs a light server that essentially acts as a Twitter timeline to RSS converter.

Accessing the web interface (e.g., running on your local machine, on `http://localhost:3000`) allows you to specify users to follow. Enter any number of Twitter display names (the @<name> bit) on new lines and hit submit.

The RSS feed is generated on demand at `/feed.xml` based on the configured users. Tweets are presented in ascending chronological order (newest first).

If you don't want to have a single feed and prefer your RSS aggregator to manage users, you can instead access a single timelineat `/:user` (e.g., `http://localhost:3000/abcnews`).

## Development

TODO: Write development instructions here

## TODO list

- [ ] Post-process body to include links to hashtags, users, etc.
- [ ] Embed media
- [ ] Manage to do all the above without the XML serialiser fucking everything
- [ ] Push extended tweet functionality to upstream

## Contributing

1. Fork it (<https://gitlab.com/xeals/aviary/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [xeals](https://github.com/xeals) - creator, maintainer

## License

aviary is licensed under the [GPL-3.0 license](LICENSE).
