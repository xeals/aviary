FROM crystallang/crystal:0.26.1

RUN apt update && \
    apt install -y libsqlite3-dev

WORKDIR /app

COPY shard.* /app/
RUN shards install

COPY . /app

RUN shards build

CMD ./bin/aviary
